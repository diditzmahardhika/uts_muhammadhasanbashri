<?php

namespace App\Http\Controllers;

use App\Models\PesananDetail;
use Illuminate\Http\Request;

class PesananDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $semua = PesananDetail::all();
        return view('tables', compact('semua'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('plus');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        PesananDetail::create ([
            'nama'=>$request->nama,
            'harga'=>$request->harga,
            'alamat'=>$request->alamat,
        ]);

        return redirect('table');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PesananDetail  $pesananDetail
     * @return \Illuminate\Http\Response
     */
    public function show(PesananDetail $id)
    {
        $satudata = PesananDetail::find($id);
        return view ('satudata', compact('satudata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PesananDetail  $pesananDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(PesananDetail $pesananDetail, $id)
    {
        $edit = PesananDetail::findorfail($id);
        return view ('edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PesananDetail  $pesananDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PesananDetail $pesananDetail,$id)
    {
        $edit = PesananDetail::findorfail($id);
        $edit->update($request->all());

        return redirect('table');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PesananDetail  $pesananDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(PesananDetail $pesananDetail,$id)
    {
        $edit = PesananDetail::findorfail($id);
        $edit->delete();

        return back();
    }
}
