<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PesananDetail;

class PesananDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PesananDetail::create ([
          'nama' => 'Muhammad Hasan Bashri',
          'harga'=> '120.000',
          'alamat'=> 'Pamoroh',
        ]);
        PesananDetail::create ([
            'nama' => 'Nur fadiluddin',
            'harga'=> '120.000',
            'alamat'=> 'Blumbungan',
          ]);
    }
}
