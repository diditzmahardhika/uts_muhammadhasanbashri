<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PesananDetailController;

Route::get('/', function () {
    return view('content');
});
Route::get('table', [PesananDetailController::class, 'index']);
Route::get('table/{id}', [PesananDetailController::class, 'show']);
Route::get('buat', [PesananDetailController::class, 'create']);
Route::post('simpan', [PesananDetailController::class, 'store']);
Route::get('edit/{id}', [PesananDetailController::class, 'edit']);
Route::post('update/{id}', [PesananDetailController::class, 'update']);
Route::get('hapus/{id}', [PesananDetailController::class, 'destroy']);

