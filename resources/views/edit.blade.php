<form action="{{ url('update', $edit->id) }}" method="post">
    {{ csrf_field() }}
    <table>
        <tr>
            <td>nama</td>
            <td>
                <input type="text" id="nama" name="nama" value="{{ $edit->nama}}">
            </td>
        </tr>
        <tr>
            <td>harga</td>
            <td>
                <input type="text" id="harga" name="harga" value="{{ $edit->harga }}">
            </td>
        </tr>
        <tr>
            <td>alamat</td>
            <td>
                <input type="text" id="alamat" name="alamat" value="{{ $edit->alamat }}">
            </td>
        </tr>
        <tr>
            <td>
                <button type="submit">Edit data</button>
            </td>
        </tr>
    </table>
</form>
